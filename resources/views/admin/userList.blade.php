@extends('layouts.app') 
@section('content')

<div class="container-fluid">

    @if (session('status'))
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a> {{ session('status')}}
    </div>
    @endif

    <div class="row">
        <div class="col-xl-12">
            <div class="text-right">
                <a href="/admin/userList/export" class="btn btn-xs btn-primary">Export</a>
            </div>
        </div>
    </div>
    <br>

    <table class="table table-light">
        <thead>
            <tr>
                <th scope="col">Sr.No.</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Mobile</th>
                <th scope="col">Address</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>

            @foreach($users as $user)

            <tr>
                <td>{{ $count++ }}</td>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->mobile }}</td>
                <td>{{ $user->address }}</td>
                <td><a href="/admin/userList/edit/{{ $user->id }}" class="btn btn-xs btn-primary">Edit</td>
                <td><a href="/admin/userList/edit/{{ $user->id }}/delete" class="btn btn-xs btn-danger">Delete</td>
            </tr>

            @endforeach
        
        </tbody>

    </table>
    <br><br>
     
    <div class="container">
            <div class="row">
                    <div class="col-xl-12">
                        <div class="text-center">
                            {{ $users->firstItem() }} - {{ $users->lastItem() }} of {{ $users->total() }}
                        </div>
                </div>
            </div>
     </div>
    <br>

    <div class="pagination">    
        {{$users->links()}}
    </div>
    
</div>
@endsection