<?php

namespace App\Http\Requests;

use Illuminate\Validation\Factory as ValidationFactory;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\CheckMail;

class UpdateUserRequest extends FormRequest
{



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->route('user_id');
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['email',new CheckMail($userId)],
            'mobile'=>['required'],
            'address'=>['required']
        ];
    }
}
