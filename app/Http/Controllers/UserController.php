<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Jobs\ExportRequest;
use App\Services\UserService;

class UserController extends Controller
{
    protected $userservice;

    public function __construct(UserService $userservice)
    {
        $this->userservice = $userservice;
    }

    public function index()
    {

        $users = $this->userservice->index();

        $count = 1;

        return view('admin.userList', compact('users', 'count'));
    }

    public function edit($id)
    {
        $users = $this->userservice->edit($id);

        return view('admin.userEdit', compact('users'));
    }

    public function update(UpdateUserRequest $request, $id)
    {

        $this->userservice->update($request, $id);

        return redirect()->route('admin.userList')->with(['status' => 'User Updated Successfully']);

    }

    public function delete($id)
    {

        $this->userservice->delete($id);

        return redirect()->route('admin.userList')->with(['status' => 'User Deleted']);

    }

    public function export()
    {

        ExportRequest::dispatch();

        return redirect()->route('admin.userList')->with(['status' => 'Request for User Data is recieved Mail will be sent in few minutes Thankyou!!']);

    }

}
