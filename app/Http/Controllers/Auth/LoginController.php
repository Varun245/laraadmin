<?php

namespace App\Http\Controllers\Auth;

use App\AuthenticationLog;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/userList';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, User $user)
    {
        $log = new AuthenticationLog;
        $log->user_id = auth()->id();
        $log->login_time = Carbon::now();
        $log->login_agent = $_SERVER['HTTP_USER_AGENT'];
        $log->ip_address = $request->getClientIp();
        $log->save();

        $authId = AuthenticationLog::where('user_id', auth()->id())->value('id');

        Session::put('id', $log->id);

    }
}
