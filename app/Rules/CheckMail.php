<?php

namespace App\Rules;

use app\User;
use Illuminate\Contracts\Validation\Rule;

class CheckMail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $customValue;

    public function __construct($customValue)
    {
        $this->customValue = $customValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $userId = $this->customValue;

        $result = User::where('email', $value);
        if ($result) {
            $result = User::where('email', $value)->value('id');
            if ($result == $userId) {
                return true;
            } else {
                $result = User::where('email', $value)->value('email');
                if ($result) {
                    return false;
                }

                return true;

            }
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email already exists';
    }
}
