<?php

namespace App\Services;

use App\Repositories\UserActivityRepository;
use App\User;

class UserActivityService
{

    public function __construct(UserActivityRepository $user)
    {
        $this->user = $user;
    }

    public function create($oldValues, $newValues,$id)
    {
        $this->user->trackUserActivity($oldValues, $newValues,$id);
    }

}
