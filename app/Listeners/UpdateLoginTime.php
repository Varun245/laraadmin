<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\AuthenticationLog;

class UpdateLoginTime
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $log=new AuthenticationLog;
        $log->user_id = auth()->id();
        $log->login_time=Carbon::now();
        $log->login_agent ="chrome";
        $log->ip_address= "12000";
        $log->save();

        
        
    }
}
