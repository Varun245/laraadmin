<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserActivity extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'field_name', 'old_value', 'modified_value', 'modified_by'];
}
