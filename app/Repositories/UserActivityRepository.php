<?php

namespace App\Repositories;

use App\User;
use Config;

class UserActivityRepository
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function trackUserActivity($oldValues, $newValues, $id)
    {

        $newValues;
        $updatedData = array_diff($newValues, $oldValues);

        $trackableFields = config('constants.FIELDS_TO_TRACK');

        $trackActivityData = [];

        foreach ($trackableFields as $key => $field) {

            if (isset($updatedData[$field])) {
                $valuesToInsert = [
                    'user_id' => $oldValues['id'],
                    'field_name' => $field,
                    'old_value' => $oldValues[$field],
                    'modified_value' => $newValues[$field],
                    'modified_by' => auth()->id(),
                ];

                $trackActivityData[] = $valuesToInsert;
            }
        }

        \App\UserActivity::insert($trackActivityData);

    }

}
