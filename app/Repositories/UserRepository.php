<?php

namespace App\Repositories;

use App\Mail\ExcelRequest;
use App\Services\UserActivityService;
use App\User;
use Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserRepository
{
    protected $user;
    protected $userActivityService;

    public function __construct(User $user, UserActivityService $userActivityService)
    {
        $this->user = $user;
        $this->userActivityService = $userActivityService;

    }

    public function all()
    {
        return $this->user->orderby('created_at', 'DESC')->paginate(10);
    }

    public function show($id)
    {
        return $this->user->find($id);
    }

    public function update($id, array $attributes)
    {

        $oldValues = $this->user->find($id)->getOriginal();

        $user = User::find($id);
        $user->fill($attributes);
        $newValues = $user->getDirty();
        $result = $user->save();

        if ($result) {
            return $this->userActivityService->create($oldValues, $newValues, $id);
        }

    }

    public function remove($id)
    {
        $this->user->where('id', $id)->delete();
    }

    public function storeToExcel()
    {

        $data = DB::table('users')->get()->toArray();

        $userArray[] = array('id', 'first_name', 'last_name', 'mobile', 'email', 'created_at');

        foreach ($data as $user) {
            $userArray[] = array(

                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'mobile' => $user->mobile,
                'email' => $user->email,
                'created_at' => $user->created_at,

            );
        }

        $id = auth()->id();

        Excel::create('UserData_' . $id, function ($excel) use ($userArray) {
            $excel->setTitle('fileName');
            $excel->sheet('fileName', function ($sheet) use ($userArray) {
                $sheet->fromArray($userArray, null, 'A1', false, false);
            });
        })->store('xls');

        $user = User::find($id)->toArray();

        Mail::to($user['email'])->send(new ExcelRequest());

    }
}
