<?php

return [
    'FIELDS_TO_TRACK' => [
        'first_name',
        'last_name',
        'email',
        'mobile'
    ]
];